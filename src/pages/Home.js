import React from 'react'

//FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";

import { Layout } from '../components/Layout'
import Jumbotron from '../components/Jumbotron'

import styled from 'styled-components'
import mernimage from '../assets/images/MERN-Stack.png'


library.add(
    faExternalLinkAlt
);

const Styles = styled.div`

    .mern {
        ${'' /* height: 15em; */}
        padding-top: 3em;
        padding-bottom: 2em;
        min-height: 45%;
        min-width: 40%;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
        max-width: 100%;
    }

    .about-me {
        margin-top: 20px;
        margin-bottom: 10vh;
        padding: 20px;
        justify-content: center;
        display: flex;
        border-radius: 10px;
        background-color: #fff;
    }

    #intro-cont {
        text-align: center;
        padding-top: 20px;
        padding: 3em;
    } 

`;

export default function Home({id}) {
    return (
        <>
            <Styles>
            <Jumbotron />
                <Layout>
                        <div className="about-me" id={id}>
                            <div className="row">
                                <div className="col-md-12 pt-5" id="intro-cont">
                                    <h2 id="about-greet" className='pt-4'>Hello World!</h2>
                                    <p className='pt-4'>
                                        My name is Nicolas Adrian Ong <small>(Nico)</small>, I am a Full Stack Web <em><small>(Javascript)</small></em> Developer from the Philippines. I aim to develop my skills through experiences and I love fixing stuff! 🔨
                                    </p>
                                </div>

                                {/* <div className="col-md-6">
                                    <img className='mern' src={mernimage} alt='mern'/>
                                </div> */}
                                
                            </div>
                        </div>
                </Layout>
            </Styles>
        </>
    )
}
