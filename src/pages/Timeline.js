import React from 'react';
import { Jumbotron } from 'react-bootstrap';

//FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBriefcase, faGraduationCap } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import timelineElements from '../data/timelineElements';
import { 
    VerticalTimeline, 
    VerticalTimelineElement 
} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import styled from 'styled-components';

library.add(
    faBriefcase,
    faGraduationCap
);

const Styles = styled.div`

    .title {
        font-size: 3em;
        text-align: center;
    }

    h3 {
        padding-top: 0.25em;
    }

    .vertical-timeline-element-content {
        box-shadow: 0 0.25em 0.5em 0 rgba(0,0,0,0.25),
        0 0.4em 1.25em 0 rgba (0,0,0,0.15) !important;
        padding: 2em 3em !important;
    }

    .date {
        color: #000;
    }

    #description {
        margin: 1.5em 0 2em 0;
    }

    #jumboTime {
        text-align: center;
    }

    @media only screen and (max-width: 2000px) {
        .vertical-timeline-elements-date {
            display: block !important;
            float: none !important;
            color: rgb(44,44,44);
            margin-top: 1.5em;
        }
    }

`;

let WorkIcon = <FontAwesomeIcon icon={['fas', 'briefcase']} size="lg" />;
let SchoolIcon = <FontAwesomeIcon icon={['fas', 'graduation-cap']} size="lg" />;

function Timeline({id}) {
    
    let workIconStyles = { background: "#06D6A0" };
    let schoolIconStyles = { background: "#F9C74F" };
    
    return (
        <div>
            <Styles>

                <Jumbotron fluid id='jumboTime'>
                    <h1 className='title' id={id}>Timeline ⌚</h1>
                    <p>Hello! These are my educational and work experiences!</p>
                </Jumbotron>
                    <VerticalTimeline>
                        {
                            timelineElements.map(e => {
                                let isWorkIcon = e.icon === 'work';

                                
                                return (
                                    <VerticalTimelineElement
                                        key={e.id}
                                        date={e.date}
                                        dateClassName='date'
                                        iconStyle={isWorkIcon ? workIconStyles : schoolIconStyles}
                                        icon={isWorkIcon ? WorkIcon : SchoolIcon}
                                    >
                                        <h3 className='vertical-timeline-element-title'>
                                            {e.title}
                                        </h3>
                                        <h5 className='vertical-timeline-element-subtitle'>
                                            {e.location}
                                        </h5>
                                        <p id='description'>{e.description}</p>

                                    </VerticalTimelineElement>
                                );
                            })}
                    </VerticalTimeline>
            </Styles>
        </div>
    );
}

export default Timeline;