import React from 'react'
import { Layout } from '../components/Layout'
import memojithumb from '../assets/images/memoji-thumb.png';
import styled from 'styled-components';

const Styles = styled.div`

#contact-area {
	margin-top: 5vh;
    margin-bottom: 20vh;
	padding: 30px;
	border-radius: 10px;
	background-color: #fff;
  	position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
}

#contact-submit {
	padding-top: 30px;
}

.hire-txt {
	font-weight: 500;
}

.map-responsive{
	overflow:hidden;
	padding-bottom:56.25%;
	position:relative;
	height:0;
}
.map-responsive iframe{
	left:0;
	top:0;
	height:100%;
	width:100%;
	position:absolute;
}

`;

export default function Contact({id}) {
    return (
        <>
        <Styles>
            <Layout>
            <div className="container" id="contact-cont">

            <div id="contact-area">

            <h2 className="hire-txt" id={id}>Hire me! <span><img src={memojithumb} width={70} height={70} alt="memoji thumb" loading="lazy"/></span></h2>

            <div className="row">

                {/* <div className="col-lg-6" id="map-side">

                    <div className="map-responsive">

                        <iframe 
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d241.33847455247036!2d121.06670991591908!3d14.575378967398635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c968f24187dd%3A0x4a5ca3a104afc241!2sEl%20Capo!5e0!3m2!1sen!2sph!4v1608034458014!5m2!1sen!2sph" width={450} height={450} frameborder={1} allowfullscreen="" aria-hidden="false" title='locmap'/>

                    </div>

                </div> */}

                <div className="col-lg-12 align-self-center">
                    <h3 className='text-center'>Lets work together! 🤝</h3>
                    <p className='text-center'><a href='mailto:nico.adrianong@gmail.com'>Click here to send me an Email! 📧</a></p>
                </div>

            </div>	

            </div>

            </div>
            </Layout>
            </Styles>
        </>
    )
}
