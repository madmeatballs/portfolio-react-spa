import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import { Layout } from '../components/Layout'
import styled from 'styled-components';
import CardProjects from '../components/CardProjects';

const Styles = styled.div`

.proj-container {
  width: 60vw;
  margin: auto;
}

`;

function Projects({id}) {
    return (
        <>
        <Styles>
            <Jumbotron>
                    <h2 className='text-center' id={id}>My Projects 🦺</h2>
            </Jumbotron>
              <Layout>
                <div className="proj-container"> 
                    <CardProjects />
                </div>    
              </Layout>
        </Styles>
        </>
    )
}

export default Projects;