import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import { Layout } from '../components/Layout'
import styled from 'styled-components';
import SkillCards from '../components/SkillCards'

const Styles = styled.div`

.skills-cont {
    background-color: white;
    border-radius: 10px;
    padding-top: 20px;
    padding-bottom: 20px;
    width: 70vw;
    margin: auto;
    margin-bottom: 40px;
}

`;

function Skills({id}) {
    return (
        <>
        <Styles>
            <Jumbotron>
                    <h2 className='text-center' id={id}>My Skills 🤹‍♀️</h2>
            </Jumbotron>
              <Layout>
                <div className="skills-cont"> 
                    <SkillCards />
                </div>    
              </Layout>
        </Styles>
        </>
    )
}

export default Skills;