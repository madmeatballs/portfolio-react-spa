import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as IoIcons from 'react-icons/io';
import * as Io5Icons from 'react-icons/io5';
import * as GiIcons from 'react-icons/gi';

export const navData =  [
    {
        title: 'Intro',
        path: 'homesec',
        icon: <Io5Icons.IoMan />,
        cName: 'nav-text'
    },
    {
        title: 'Skills',
        path: 'skills',
        icon: <GiIcons.GiSkills />,
        cName: 'nav-text'
    },
    {
        title: 'Timeline',
        path: 'timeline',
        icon: <GiIcons.GiLifeBar />,
        cName: 'nav-text'
    },
    {
        title: 'Projects',
        path: 'projects',
        icon: <FaIcons.FaProjectDiagram />,
        cName: 'nav-text'
    },
    {
        title: 'Contact',
        path: 'contact',
        icon: <IoIcons.IoIosContact />,
        cName: 'nav-text'
    }
]


