let timelineElements = [
    {
        id: 1,
        title: 'Full-Stack Web Developer',
        location: 'Flowerstore.ph (Blue Aurora Solutions Inc.)',
        description: 'Working as a Full Stack Web Developer. Managed PHP Laravel/Vue 3 project. Focusing on Mobile App Development using React-Native.',
        buttonText: 'Company Website',
        date: 'June 2021 - Present',
        icon: 'work'
    },
    {
        id: 2,
        title: 'Full-Stack Web Development (MERN)',
        location: 'Zuitt Coding Bootcamp Philippines',
        description: 'Bootcamp for Full-Stack Javascript Development focused on MERN Stack.',
        buttonText: 'School Website',
        date: 'December 2020 - April 2021',
        icon: 'school'
    },
    {
        id: 3,
        title: 'Restaurateur',
        location: `El Capo's Italian Kitchenette`,
        description: 'Business Venture for B.S. Entrepreneurship for Entrepreneurs School of Asia',
        buttonText: 'Company Website',
        date: 'January 2018 - April 2021',
        icon: 'work'
    },
    {
        id: 4,
        title: 'IT Manager',
        location: 'Premier Ready Mix Inc.',
        description: 'Managed custom company software and hardware.',
        buttonText: 'Company Website',
        date: 'July 2016 - September 2017',
        icon: 'work'
    },
    {
        id: 5,
        title: 'College',
        location: 'Entrepreneurs School of Asia (Thames International School)',
        description: 'Graduated B.S. Entrepreneurship',
        buttonText: 'School Website',
        date: 'June 2010 - July 2018',
        icon: 'school'
    },
    {
        id: 6,
        title: 'Grade School - High School',
        location: 'Lourdes School of Mandaluyong',
        description: 'Graduated Grade School and High School',
        buttonText: 'School Website',
        date: 'April 1998 - March 2010',
        icon: 'school'
    }
]

export default timelineElements;