import * as FaIcons from "react-icons/fa";
import * as SiIcons from "react-icons/si";


let cardSkills = [
    {
        tech: 'Javascript',
        image: <FaIcons.FaJsSquare fontSize="4em"/>
    },
    {
        tech: 'HTML5',
        image: <FaIcons.FaHtml5 fontSize="4em"/>
    },
    {
        tech: 'CSS3',
        image: <FaIcons.FaCss3Alt fontSize="4em"/>
    },
    {
        tech: 'React.js',
        image: <FaIcons.FaReact fontSize="4em"/>,
    },
    {
        tech: 'Next.js',
        image: <SiIcons.SiNextDotJs fontSize="4em"/>
    },
    {
        tech: 'MongoDB',
        image: <SiIcons.SiMongodb fontSize="4em"/>
    },
    {
        tech: 'Node.js',
        image: <FaIcons.FaNodeJs fontSize="4em"/>
    },
    {
        tech: 'styled-components',
        image: <SiIcons.SiStyledComponents fontSize="4em"/>
    },
    {
        tech: 'Bootstrap',
        image: <FaIcons.FaBootstrap fontSize="4em"/>
    },
    {
        tech: 'Git',
        image: <FaIcons.FaGitAlt fontSize="4em"/>
    },
    {
        tech: 'Adobe Photoshop',
        image: <SiIcons.SiAdobephotoshop fontSize="4em"/>
    },
    {
        tech: 'Light Adobe Premiere',
        image: <SiIcons.SiAdobepremiere fontSize="4em"/>
    },
    {
        tech: 'Light Java',
        image: <FaIcons.FaJava fontSize="4em"/>
    },
    {
        tech: 'React-Native',
        image: <FaIcons.FaReact fontSize="4em"/>
    },
    {
        tech: 'Expo',
        image: <SiIcons.SiExpo fontSize="4em"/>
    },
    {
        tech: 'PHP Laravel',
        image: <FaIcons.FaLaravel fontSize="4em"/>
    },
    {
        tech: 'Vue 3',
        image: <FaIcons.FaVuejs fontSize="4em"/>
    },
    {
        tech: 'MySql',
        image: <SiIcons.SiMysql fontSize="4em"/>
    }
]

export default cardSkills;