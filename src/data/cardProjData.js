import codetron from '../assets/images/codetron.png'
import budgettracker from '../assets/images/budgettracker.png'

let cardProjData = [
    {
        title: 'Codetron Coding School',
        photo: codetron,
        status: 'done',
        text: 'This is my Capstone 2 project for Zuitt Bootcamp',
        link: 'https://madmeatballs.gitlab.io/cs2_booking_system_fe/index.html',
        tech: 'Vanilla Javascript, Node.js, Express.js, Mongoose, MongoDB, Boostrap'
    },
    {
        title: 'Sabby! - Budget Tracker',
        status: 'done',
        photo: budgettracker,
        text: 'This is my Capstone 3 project for Zuitt Bootcamp; To access, use email: joe@mail.com and password: 1234.',
        link: 'https://budget-tracker-cs3-rho.vercel.app/',
        tech: 'Next.js, Node.js, Express.js, Mongoose, MongoDB, Bootstrap'
    },
    {
        title: 'Assembly App',
        status: 'done',
        photo: null,
        text: 'An app I made for a my current job. This app uses a barcode scanner and updates items via an external API.',
        link: 'confidential',
        tech: 'Expo, React Native'
    },
    {
        title: 'e-Commerce App',
        status: 'wip',
        photo: null,
        text: 'An e-commerce app for android and iOS using react-native, development is still on-going.',
        link: 'confidential',
        tech: 'React Native'
    }
]

export default cardProjData;