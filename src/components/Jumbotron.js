import React from 'react';
import { Jumbotron as Jumbo, Container} from 'react-bootstrap';

//FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";

import memojihand from '../assets/images/memoji-hand.png'
import styled from 'styled-components';

library.add(
    faExternalLinkAlt
);

const Styles = styled.div`
    ${'' /* .jumbo {
        background: url(${imagehere}) no-repeat fixed bottom;
        background-size: cover;
        color: #efefef;
        height: 200px;
        position: relative;
        z-index: -2;
    } */}

    .jumbo {
        background-color: #BFC0C0;
        color: black;
        display: flex;
        height: 25%;
    }

    ${'' /* .overlay {
        background-color: #000;
        opacity: 0.3;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: -1;
    } */}

`;


export default function Jumbotron() {
    return (
        <Styles>
            <Jumbo fluid className='jumbo'>
                    <div className='overlay'></div>
                    <Container className='pt-5'>
                        <img src={memojihand} width="100" height="100" alt="memoji hand" loading="lazy"/>   
                        <h1>Nicolas Adrian Ong</h1>
                        <p>
                        Full Stack Web Developer <small><em>(React-Native, Vue 3, PHP Laravel, MySQL, MongoDB, Express, React, and Node. js)</em></small>
                        </p>
                    </Container>
            </Jumbo>
        </Styles>
    )
}