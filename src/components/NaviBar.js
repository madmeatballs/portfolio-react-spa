import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { Link, animateScroll as scroll } from "react-scroll";
import styled from 'styled-components'

const Styles = styled.div`
    .navbar {
        background-color: #2D3142;
    }

    .navbar-brand, .navbar-nav .nav-link {
        color: #fff;

        &:hover {
            color: orange;
        }
    }

`;

export default function NaviBar() {
    const scrollToTop = () => {
        scroll.scrollToTop();
      };

    return (
        <Styles>
        <Navbar variant='dark' expand='lg' className='sticky-top'>
          <Navbar.Brand onClick={scrollToTop}>
            Hello!
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className='ml-auto'>
              <Nav.Link>
              <Link
                activeClass="active"
                to="homesec"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                Intro
              </Link>
              </Nav.Link>
              <Nav.Link>
              <Link
                activeClass="active"
                to="timeline"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                Timeline
              </Link>
              </Nav.Link>
              <Nav.Link>
              <Link
                activeClass="active"
                to="projects"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                Projects
              </Link>
              </Nav.Link>
              <Nav.Link>
              <Link
                activeClass="active"
                to="contact"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                Contact
              </Link>
              </Nav.Link>
          </Nav>
          </Navbar.Collapse>
      </Navbar>
      </Styles>
    )

}