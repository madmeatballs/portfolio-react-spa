import { useState, useEffect } from 'react'
import { animateScroll as scroll } from 'react-scroll';
import * as FaIcons from 'react-icons/fa';
import styled from 'styled-components';

const Styles = styled.div`

    .toTopBtn {
        z-index: 999;
        position: fixed;
        background-color: transparent;
        color:  #e39b00;
        width: 50px;
        height: 50px;
        bottom: 20px;
        right: 30px;
        border-radius: 100%;
        font-size: 24px;
        padding: 2px;
        border: solid #e39b00;
        text-align: center;
        pointer-events: none;
        opacity: 0;
        transition: all .4s;
    }

    .toTopBtn.active {
        pointer-events: auto;
        opacity: 1;
    }

    .toTopBtn:hover {
        transform: '0.3s';
        color: #ececec;
        background-color: #e39b00;
    }

`
    
const scrollToTop = () => {
    scroll.scrollToTop();
  };


export default function ScrollTop({showBelow}) {

    const [show, setShow] = useState('')

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.pageYOffset > 60) {
                setShow('active');
            } else {
                setShow(null);
            }
        })
    })

    return (
        <Styles>
            <div className={`toTopBtn ${show}`} onClick={scrollToTop}>
                    <FaIcons.FaChevronUp />
            </div>
        </Styles>
    )
}
