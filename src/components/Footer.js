import React from 'react';

import styled from 'styled-components';

const Styles = styled.div`

    .footer {
    width: 100%;
    border-top: 1px solid #eaeaea;
    text-align: center;
    color: white;
    background-color: #4F5D75;
    }

`;

export default function Footer() {
    return(
        <>
            <Styles>
                <footer className="footer position-sticky pt-4 pb-2">

                    <p><a id="footer-name" href="https://www.linkedin.com/in/ongnico/" target="_blank" rel="noreferrer">
                    Nicolas Adrian Ong </a>
                    &copy; Copyright {new Date().getFullYear()}</p>
                    <p><small>This portfolio is made with React.js and some happiness! 😉</small></p>

                </footer>
            </Styles>
        </>
    )
}