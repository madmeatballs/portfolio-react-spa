import React from 'react';
import { Card, Button, Badge } from 'react-bootstrap';
import cardProjData from '../data/cardProjData';
import styled from 'styled-components';

const Styles = styled.div`

`

export default function CardProjects() {
    
    function showStatus() {
        return (
            <Badge variant="info" className='ml-2 pt-1'>Work-In-Progress</Badge>
        )
    }

    return (
        <>
            <Styles>
                <div className='cards-container'>
                    {cardProjData.map((item, index) => {
                        return (
                            <>
                            <br/>
                            <Card key={index} className='text-center'>
                            <Card.Header>{item.title}
                                { item.status === 'wip' ? showStatus() : null }
                            </Card.Header>
                            <Card.Img variant="top" src={item.photo} />
                                <Card.Body>
                                    <Card.Text>
                                        {item.text}
                                    </Card.Text>
                                    {
                                        item.link === 'confidential' ?
                                        <Button variant="dark" disabled>
                                            Confidential
                                        </Button>
                                        :
                                        <Button href={item.link} target='_blank' variant="dark">Hosted Website</Button>
                                    }
                                </Card.Body>
                                <Card.Footer className="text-muted">{item.tech}</Card.Footer>
                            </Card>
                            <br/>
                            </>
                        )
                    })}
                </div>
            </Styles>
        </>
    )
}
