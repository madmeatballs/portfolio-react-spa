import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
// import { Link } from 'react-router-dom';
import { Link } from "react-scroll";
import { navData } from '../data/Sidenav';
import styled from 'styled-components';
import { IconContext } from 'react-icons'

const Styles = styled.div`

    ${'' /* .sidebar {
        background-color: #2D3142;
        height: 60px;
        display: flex;
        justify-content: start;
        align-items: center;
        position: fixed;
        opacity: 0.9;
        width:100%;
        z-index: 999;
    } */}

    .sidebar {
        background-color: #2D3142;
        height: 60px;
        display: flex;
        justify-content: start;
        align-items: center;
        position: fixed;
        opacity: 0.9;
        width:100%;
        z-index: 999;
    }

    .menu-bars {
        margin-left: 2rem;
        font-size: 2rem;
        background: none;
    }

    .nav-menu {
        background-color: #2D3142;
        width: 250px;
        height: 100vh;
        display: flex;
        justify-content: center;
        position: fixed;
        opacity: 0.9;
        top: 0;
        left: -100%;
        transition: 850ms;
        z-index: 999;
        
    }

    .nav-menu.active {
        left: 0;
        transition: 350ms;
    }

    .nav-text {
        display: flex;
        justify-content: start;
        align-items: center;
        padding: 8px 0px 8px 16px;
        list-style: none;
        height: 60px;
    }

    .nav-text a {
        text-decoration: none;
        color: #fff;
        font-size: 18px;
        width: 95%;
        height: 100%;
        display: flex;
        align-items: center;
        padding: 0 16px;
        border-radius: 4px;
    }

    .nav-text a:hover {
        background-color: #e39b00;
    }

    .nav-menu-items {
        width: 100%;
    }

    .navbar-toggle {
        background-color: #2D3142;
        width: 100%;
        height: 80px;
        display: flex;
        justify-content: start;
        align-items: center;
    }

    span {
        margin-left: 16px;
    }
`;

export default function SideNav() {
    
    const [ sidebar, setSidebar ] = useState(false);
    const showSidebar = () => setSidebar(!sidebar);

    return (
        <>
        
        <Styles>
        <IconContext.Provider value={{color: 'orange'}}>
        <div className="sidebar">
            <Link to='#' className='menu-bars'>
                <FaIcons.FaBars onClick={showSidebar} />
            </Link>
        </div>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
            <ul className='nav-menu-items'>
                <li className='navbar-toggle'>
                    <Link to='#' className='menu-bars' onClick={showSidebar}>
                        <AiIcons.AiOutlineClose />
                    </Link>
                </li>
                {navData.map((item, index) => {
                    return (
                        <li key={index} className={item.cName}>
                            <Link 
                                to={item.path}
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={500}
                                onClick={showSidebar}>
                                {item.icon}
                                <span>{item.title}</span>
                            </Link>
                        </li>
                    )
                })}
            </ul>
        </nav>
        </IconContext.Provider>
        </Styles>
        
        </>
    )
}