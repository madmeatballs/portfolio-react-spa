import React from 'react';
import { Table } from 'react-bootstrap';
import styled from 'styled-components';
import cardSkills from '../data/cardSkills'

const Styles = styled.div`

.table-cont {
    margin: auto;
}

.name {
    text-align: center;
}

.icons {
    text-align: right;
}

.text-icon {
    text-align: right;
    font-size: 1.5em;
}

`

export default function SkillCards() {
    
    function createTable() {
        return(
            cardSkills.map((item, index) => {
                return (

                <tr key={index}>
                    <td className='icons'>{item.image}</td>
                    <td className='name'>{item.tech}</td>
                </tr>

                )
            })
        )
    }

    return (

        <>
            <Styles>
                <div className='table-cont'>
                <Table borderless size='sm' className='text-center'>
                    <tbody> 
                        {createTable()}
                        <tr key='express'>
                            <td className='text-icon'>Express</td>
                            <td className='name'>Express.js</td>
                        </tr>
                    </tbody>
                </Table>
                </div>
            </Styles>
        </>

    )
}
