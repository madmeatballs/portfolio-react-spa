import React from 'react';

import Home from './pages/Home';
import Projects from './pages/Projects';
import Contact from './pages/Contact';
import Timeline from './pages/Timeline';
import Skills from './pages/Skills';
// import Error from './pages/Error';

// import NaviBar from './components/NaviBar';
import SideNav from './components/SideNav';
import Footer from './components/Footer';
import ToTop from './components/toTop';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';



function App() {
  return (
    <>
    {/* <Router> */}
      <SideNav />
      <Home id='homesec' />
      <Skills id='skills' />
      <Timeline id='timeline' />
      <Projects id='projects' />
      <Contact id='contact' />
      <ToTop />
      <Footer />
    {/* </Router> */}
    </>
  );
}

export default App;
